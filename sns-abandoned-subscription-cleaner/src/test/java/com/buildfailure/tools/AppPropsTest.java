package com.buildfailure.tools;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AppPropsTest {

    private final AppProps appProps = new AppProps();

    @Test
    public void shouldReturnDryRunAsDefault(){
        assertTrue(appProps.isDryRun());
    }
}
