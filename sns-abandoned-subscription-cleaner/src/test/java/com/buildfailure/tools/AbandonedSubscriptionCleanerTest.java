package com.buildfailure.tools;

import org.junit.Test;
import java.util.Set;

import static java.util.Collections.singleton;
import static org.mockito.Mockito.*;

public class AbandonedSubscriptionCleanerTest {

    private final AbandonedSubscriptionFinder abandonedSubscriptionFinder = mock(AbandonedSubscriptionFinder.class);

    private final SubscriptionRemover subscriptionRemover = mock(SubscriptionRemover.class);

    private final AbandonedSubscriptionCleaner abandonedSubscriptionCleaner = new AbandonedSubscriptionCleaner(
            abandonedSubscriptionFinder,
            subscriptionRemover
    );

    private final Set<String> subscriptions = singleton("foo:bar");

    @Test
    public void shouldCleanAbandonedSubscriptions(){
        when(abandonedSubscriptionFinder.find()).thenReturn(subscriptions);

        abandonedSubscriptionCleaner.clean();

        verify(subscriptionRemover).remove(subscriptions);
    }
}
