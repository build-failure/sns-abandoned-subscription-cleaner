package com.buildfailure.tools;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.mockito.Mockito.*;

@Ignore
public class SnsAbandonedSubscriptionCleanerIT {

  private final Context context = mock(Context.class);

  private final LambdaLogger logger = mock(LambdaLogger.class);

  private final ScheduledEvent event = mock(ScheduledEvent.class);

  private static final String REGION = "us-west-2";

  @Before
  public void init(){
    when(context.getLogger()).thenReturn(logger);
    when(event.getRegion()).thenReturn(REGION);

    doAnswer(invocation -> {
      System.out.println(Arrays.stream(invocation.getArguments()).findFirst().get());
      return null;
    }).when(logger).log(Mockito.anyString());
  }

  @Test
  public void shouldCleanupAbandonedSubscriptions() {
    SnsAbandonedSubscriptionCleaner snsAbandonedSubscriptionCleaner = new SnsAbandonedSubscriptionCleaner();
    snsAbandonedSubscriptionCleaner.handleRequest(event, context);
  }
}
