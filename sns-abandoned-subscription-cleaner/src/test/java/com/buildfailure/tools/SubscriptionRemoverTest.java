package com.buildfailure.tools;

import org.junit.Test;

import static java.util.Collections.singleton;
import static org.mockito.Mockito.*;

public class SubscriptionRemoverTest {

    private static final String SUBSCRIPTION_ARN = "foo:bar";

    private final AppProps appProps = mock(AppProps.class);

    private final Logger logger = mock(Logger.class);

    private final AwsClient awsClient = mock(AwsClient.class);

    private final SubscriptionRemover subscriptionRemover = new SubscriptionRemover(appProps, logger, awsClient);

    @Test
    public void shouldRemove(){
        when(appProps.isDryRun()).thenReturn(false);

        subscriptionRemover.remove(singleton(SUBSCRIPTION_ARN));

        verify(awsClient).removeSubscription(SUBSCRIPTION_ARN);
    }

    @Test
    public void shouldSkipRemoval(){
        when(appProps.isDryRun()).thenReturn(true);

        subscriptionRemover.remove(singleton(SUBSCRIPTION_ARN));

        verify(awsClient, times(0)).removeSubscription(SUBSCRIPTION_ARN);
    }
}
