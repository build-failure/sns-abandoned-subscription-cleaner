package com.buildfailure.tools;

import org.junit.Test;
import java.util.Set;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AbandonedSubscriptionFinderTest {

    private static final String SUBSCRIPTION_ARN = "foo:bar";

    private static final String TOPIC_ARN = "foz:baz";

    private final Logger logger = mock(Logger.class);

    private final AwsClient awsClient = mock(AwsClient.class);

    private final AwsClient.Subscription subscription = mock(AwsClient.Subscription.class);

    private final AbandonedSubscriptionFinder abandonedSubscriptionFinder = new AbandonedSubscriptionFinder(logger, awsClient);

    @Test
    public void shouldFindAbandonedSubscriptions(){
        when(awsClient.getSubscriptions()).thenReturn(singletonList(subscription));
        when(awsClient.isTopicExist(TOPIC_ARN)).thenReturn(false);
        when(subscription.getSubscriptionArn()).thenReturn(SUBSCRIPTION_ARN);

        Set<String> subscriptions = abandonedSubscriptionFinder.find();

        assertEquals(subscriptions, singleton(SUBSCRIPTION_ARN));
    }
}
