package com.buildfailure.tools;

import java.util.Set;

public class AbandonedSubscriptionCleaner {

    private final AbandonedSubscriptionFinder abandonedSubscriptionFinder;

    private final SubscriptionRemover subscriptionRemover;

    public AbandonedSubscriptionCleaner(
            AbandonedSubscriptionFinder abandonedSubscriptionFinder,
            SubscriptionRemover subscriptionRemover
    ) {
        this.abandonedSubscriptionFinder = abandonedSubscriptionFinder;
        this.subscriptionRemover = subscriptionRemover;
    }

    public void clean(){
        Set<String> abandonedSubscriptions = abandonedSubscriptionFinder.find();

        subscriptionRemover.remove(abandonedSubscriptions);
    }
}
