package com.buildfailure.tools;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;
import software.amazon.awssdk.services.sns.SnsClient;

import static software.amazon.awssdk.regions.Region.of;

public class SnsAbandonedSubscriptionCleaner implements RequestHandler<ScheduledEvent, Void> {

    private AbandonedSubscriptionCleaner abandonedSubscriptionCleaner;

    public Void handleRequest(final ScheduledEvent input, final Context context) {
        init(input, context);

        abandonedSubscriptionCleaner.clean();

        return null;
    }

    private void init(final ScheduledEvent input, final Context context){
        SnsClient snsClient = SnsClient.builder()
                .region(of(input.getRegion()))
                .build();
        Logger logger = new Logger(context.getLogger());
        AppProps appProps = new AppProps();
        AwsClient awsClient = new AwsClient(snsClient);

        this.abandonedSubscriptionCleaner = new AbandonedSubscriptionCleaner(
                new AbandonedSubscriptionFinder(logger, awsClient),
                new SubscriptionRemover(appProps, logger, awsClient)
        );
    }
}
