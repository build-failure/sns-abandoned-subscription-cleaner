package com.buildfailure.tools;

import java.util.Collection;

import static java.text.MessageFormat.format;

public class SubscriptionRemover {

    private final AppProps appProps;

    private final Logger logger;

    private final AwsClient awsClient;

    public SubscriptionRemover(
            AppProps appProps,
            Logger logger,
            AwsClient awsClient
    ) {
        this.appProps = appProps;
        this.logger = logger;
        this.awsClient = awsClient;
    }

    public void remove(Collection<String> subscriptions){
        if(appProps.isDryRun()){
            logger.log("Dry run mode. Skipping subscription cleanup.");
        } else {
            subscriptions.forEach(this::remove);
        }
    }

    private void remove(String subscription){
        logger.log(format("Removing subscription {0}.", subscription));
        awsClient.removeSubscription(subscription);
    }
}
