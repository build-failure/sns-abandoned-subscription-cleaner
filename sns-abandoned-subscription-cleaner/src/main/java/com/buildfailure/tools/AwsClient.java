package com.buildfailure.tools;

import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.*;

import java.util.List;
import java.util.stream.Collectors;

public class AwsClient {

    private final SnsClient snsClient;

    public AwsClient(SnsClient snsClient) {
        this.snsClient = snsClient;
    }

    public List<Subscription> getSubscriptions(){
        return snsClient.listSubscriptionsPaginator()
                .stream()
                .map(ListSubscriptionsResponse::subscriptions)
                .flatMap(List::stream)
                .map(this::map)
                .collect(Collectors.toList());
    }

    private Subscription map(software.amazon.awssdk.services.sns.model.Subscription source){
        return new Subscription(source.subscriptionArn(), source.topicArn());
    }

    public boolean isTopicExist(String topicArn){
        try {
            snsClient.getTopicAttributes(GetTopicAttributesRequest.builder().topicArn(topicArn).build());

            return true;
        } catch (NotFoundException e){
            return false;
        }
    }

    public void removeSubscription(String subscription){
        UnsubscribeRequest request = UnsubscribeRequest.builder()
                .subscriptionArn(subscription)
                .build();
        snsClient.unsubscribe(request);
    }

    public static class Subscription {

        private final String subscriptionArn;

        private final String topicArn;

        public Subscription(String subscriptionArn, String topicArn) {
            this.subscriptionArn = subscriptionArn;
            this.topicArn = topicArn;
        }

        public String getSubscriptionArn() {
            return subscriptionArn;
        }

        public String getTopicArn() {
            return topicArn;
        }
    }
}
