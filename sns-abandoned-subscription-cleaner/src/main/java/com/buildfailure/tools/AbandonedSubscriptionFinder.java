package com.buildfailure.tools;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.text.MessageFormat.format;

public class AbandonedSubscriptionFinder {

    private final Logger logger;

    private final AwsClient awsClient;

    private final Map<String, Boolean> topicMap;

    private final Set<String> abandonedSubscriptions;

    public AbandonedSubscriptionFinder(
            Logger logger,
            AwsClient awsClient
    ) {
        this.logger = logger;
        this.awsClient = awsClient;
        this.topicMap = new HashMap<>();
        this.abandonedSubscriptions = new HashSet<>();
    }

    public Set<String> find(){
        awsClient.getSubscriptions().forEach(this::find);

        logFindings();

        return abandonedSubscriptions;
    }

    private void find(AwsClient.Subscription subscription){
        String topicArn = subscription.getTopicArn();

        Boolean topicExists = topicMap.get(topicArn);

        if(topicExists == null){
            topicExists = isTopicExist(topicArn);
            topicMap.put(topicArn, topicExists);
        }

        if(!topicExists){
            abandonedSubscriptions.add(subscription.getSubscriptionArn());
        }
    }

    private boolean isTopicExist(String topicArn){
        logger.log(format("Checking topic {0}.", topicArn));
        return awsClient.isTopicExist(topicArn);
    }

    private void logFindings(){
        if(abandonedSubscriptions.isEmpty()){
            logger.log("No abandoned subscriptions found.");
        } else {
            logger.log(format("Found {0} abandoned subscriptions: {1}.", abandonedSubscriptions.size(), abandonedSubscriptions));
        }
    }
}
