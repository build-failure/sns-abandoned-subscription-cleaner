package com.buildfailure.tools;

import static java.lang.Boolean.parseBoolean;
import static java.lang.System.getenv;

public class AppProps {

    public boolean isDryRun(){
        String dryRunStr = getenv("DRY_RUN");
        if(dryRunStr == null || dryRunStr.isEmpty()){
            return true;
        }
        return parseBoolean(dryRunStr);
    }
}
